# polybar

A fast and easy-to-use status bar

https://github.com/polybar/polybar

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/i3-packages/polybar.git
```

Polybar dependencies:

i3-wm
<br>
ttf-unifont
<br>
siji-git
<br>
xorg-fonts-misc
<br>

